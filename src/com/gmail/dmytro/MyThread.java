package com.gmail.dmytro;

import java.math.BigInteger;

public class MyThread implements Runnable {

	private int item;

	MyThread(int item) {
		this.item = item;
	}
	
    @Override
	public void run() {
		BigInteger res = BigInteger.ONE;
		for (long i = 2; i <= item; i++) 
		res=res.multiply(BigInteger.valueOf(i));
		System.out.println(item+"! = "+res);
	}
}
